# Copyright 2022 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require github [ user=OpenSC release=${PNV} suffix=tar.gz ]

SUMMARY="Higher-level interface to access PKCS#11 objects"
DESCRIPTION="
The PKCS#11 API is an abstract API to perform operations on cryptographic objects such as private
keys, without requiring access to the objects themselves. That is, it provides a logical separation
of the keys from the operations. The PKCS #11 API is mainly used to access objects in smart cards
and Hardware or Software Security Modules (HSMs). That is because in these modules the cryptographic
keys are isolated in hardware or software and are not made available to the applications using them.

PKCS#11 API is an OASIS standard and it is supported by various hardware and software vendors.
Usually, hardware vendors provide a PKCS#11 module to access their devices. A prominent example is
the OpenSC PKCS #11 module which provides access to a variety of smart cards. Other libraries like
NSS or GnuTLS already take advantage of PKCS #11 to access cryptographic objects."


LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

src_configure() {
    local myconf=(
        --disable-static
    )

    enginesdir=/usr/$(exhost --target)/lib
    if option providers:openssl; then
        enginesdir=$(openssl version -e|awk -F\" '{print $2}')
    fi

    export enginesdir
    econf "${myconf[@]}" \
        --with-enginesdir="${enginesdir}"
}

src_install() {
    edo mkdir -p "${IMAGE}${enginesdir}"
    default
}

