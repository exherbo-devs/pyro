# Copyright 2016 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require github [ user=Maproom tag=V_${PV} ] cmake
require freedesktop-desktop gtk-icon-cache

SUMMARY="QMapShack is the next generation of the famous QLandkarte GT application"
DESCRIPTION="
Use QMapShack to plan your next outdoor trip or to visualize and archive all the
GPS recordings of your past exciting adventures.
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        app-arch/quazip[>=1.0][providers:qt5]
        app-gps/routino
        sci-libs/gdal
        sci-libs/proj[>=8.0.0]
        x11-libs/qtbase:5[>=5.15.2][gui][sql]
        x11-libs/qtdeclarative:5
        x11-libs/qtscript:5
        x11-libs/qttools:5
        x11-libs/qtwebengine:5
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DEXEC_INSTALL_PREFIX:PATH=/usr/$(exhost --target)
    -DCMAKE_INSTALL_PREFIX:PATH=/usr
    -DSYSCONF_INSTALL_DIR:PATH=/etc
)

src_prepare() {
    cmake_src_prepare

    # Why have all the nice options and then force your default values? Long term
    # solution here is to move to GNUInstallDirs
    edo sed -e "/FORCE$/d" -i cmake/Modules/DefineInstallationPaths.cmake
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

